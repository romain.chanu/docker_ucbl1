Role Name
=========

Install and configure docker for UCBL1 environment


Requirement
-------------

This role needs privilege escalation. You have
to set `become: yes` in your playbook and be sure
your **remote_user** is allowed.


Role Variables
--------------

Set a list of users allowed to use docker. All users
will be added to docker group.

    vars:
        users:
            - username1
            - username2

Default task behavior is to continue if you try to an absent user to docker group.
Set `allow_absent_user` to `no` if you wish to stop your
playbook on fail event.

    vars:
        allow_absent_user: no

If your OS is cloud ready, the distrubtion user will be
automaticly added to docker group.
Set `distrubtion_user` to false if you wish to disable
this behavior

    vars:
        distrubtion_user: false

If your OS does not have any distrubtion user the role won't fail.


Example Playbook
----------------


    - hosts: servers
      become: yes
      roles:
        - role: docker_ucbl1
          vars:
          users:
            - username1
            - username2

Quick start
-----------

    ansible-galaxy install git+https://forge.univ-lyon1.fr/romain.chanu/docker_ucbl1.git
    echo "---
    - name: Quick start playbook
      hosts: all
      become: yes
      roles:
        - role: docker_ucbl1
          vars:
            users:
              - user1
              - user2
    ...

        " > playbook.yml
    ansible-playbook -i YOURIPADDRESS, playbook.yml

License
-------
one line to give the program's name and a brief description
Copyright © 2021 yourname

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Author Information
------------------

Romain CHANU
UCBL1
